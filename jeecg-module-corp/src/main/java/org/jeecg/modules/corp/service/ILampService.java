package org.jeecg.modules.corp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jeecg.modules.corp.entity.Lamp;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description: tb_lamp
 * @Author: jeecg-boot
 * @Date:   2022-09-08
 * @Version: V1.0
 */
public interface ILampService extends IService<Lamp> {

    public void sendControlPackage(String id, List<Number> contentList) throws JsonProcessingException;
}
