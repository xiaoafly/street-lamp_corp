package org.jeecg.modules.corp.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.corp.entity.LightPole;
import org.jeecg.modules.corp.service.ILightPoleService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 灯杆管理
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
@Api(tags="灯杆管理")
@RestController
@RequestMapping("/corp/lightPole")
@Slf4j
public class LightPoleController extends JeecgController<LightPole, ILightPoleService> {
	@Autowired
	private ILightPoleService lightPoleService;
	
	/**
	 * 分页列表查询
	 *
	 * @param lightPole
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "灯杆管理-分页列表查询")
	@ApiOperation(value="灯杆管理-分页列表查询", notes="灯杆管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<LightPole>> queryPageList(LightPole lightPole,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<LightPole> queryWrapper = QueryGenerator.initQueryWrapper(lightPole, req.getParameterMap());
		Page<LightPole> page = new Page<LightPole>(pageNo, pageSize);
		IPage<LightPole> pageList = lightPoleService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param lightPole
	 * @return
	 */
	@AutoLog(value = "灯杆管理-添加")
	@ApiOperation(value="灯杆管理-添加", notes="灯杆管理-添加")
	//@RequiresPermissions("org.jeecg.modules:tb_light_pole:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody LightPole lightPole) {
		lightPoleService.save(lightPole);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param lightPole
	 * @return
	 */
	@AutoLog(value = "灯杆管理-编辑")
	@ApiOperation(value="灯杆管理-编辑", notes="灯杆管理-编辑")
	//@RequiresPermissions("org.jeecg.modules:tb_light_pole:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody LightPole lightPole) {
		lightPoleService.updateById(lightPole);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "灯杆管理-通过id删除")
	@ApiOperation(value="灯杆管理-通过id删除", notes="灯杆管理-通过id删除")
	//@RequiresPermissions("org.jeecg.modules:tb_light_pole:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		lightPoleService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "灯杆管理-批量删除")
	@ApiOperation(value="灯杆管理-批量删除", notes="灯杆管理-批量删除")
	//@RequiresPermissions("org.jeecg.modules:tb_light_pole:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.lightPoleService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "灯杆管理-通过id查询")
	@ApiOperation(value="灯杆管理-通过id查询", notes="灯杆管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<LightPole> queryById(@RequestParam(name="id",required=true) String id) {
		LightPole lightPole = lightPoleService.getById(id);
		if(lightPole==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(lightPole);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param lightPole
    */
    //@RequiresPermissions("org.jeecg.modules:tb_light_pole:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, LightPole lightPole) {
        return super.exportXls(request, lightPole, LightPole.class, "灯杆管理");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_light_pole:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, LightPole.class);
    }

}
