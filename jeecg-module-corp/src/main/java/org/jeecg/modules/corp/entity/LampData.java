package org.jeecg.modules.corp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: tb_lamp_data
 * @Author: jeecg-boot
 * @Date:   2022-11-04
 * @Version: V1.0
 */
@Data
@TableName("tb_lamp_data")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_lamp_data对象", description="tb_lamp_data")
public class LampData implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**照明设备id*/
	@Excel(name = "照明设备id", width = 15)
    @ApiModelProperty(value = "照明设备id")
    private java.lang.String lampId;
	/**当前亮度状态: 1-100*/
	@Excel(name = "当前亮度状态: 1-100", width = 15)
    @ApiModelProperty(value = "当前亮度状态: 1-100")
    private java.lang.Integer bri;
	/**实时电压（V 伏）*/
	@Excel(name = "实时电压（V 伏）", width = 15)
    @ApiModelProperty(value = "实时电压（V 伏）")
    private java.math.BigDecimal volt;
	/**实时电流（A 安）*/
	@Excel(name = "实时电流（A 安）", width = 15)
    @ApiModelProperty(value = "实时电流（A 安）")
    private java.math.BigDecimal curr;
	/**有功功率（W 瓦）*/
	@Excel(name = "有功功率（W 瓦）", width = 15)
    @ApiModelProperty(value = "有功功率（W 瓦）")
    private java.math.BigDecimal actp;
	/**设备SIM卡标识号*/
	@Excel(name = "设备SIM卡标识号", width = 15)
    @ApiModelProperty(value = "设备SIM卡标识号")
    private java.lang.String imsi;
	/**开关状态：0.关; 1 开*/
	@Excel(name = "开关状态：0.关; 1 开", width = 15)
    @ApiModelProperty(value = "开关状态：0.关; 1 开")
    private java.lang.String onStatus;
	/**硬件版本号*/
	@Excel(name = "硬件版本号", width = 15)
    @ApiModelProperty(value = "硬件版本号")
    private java.lang.String hver;
	/**软件版本号*/
	@Excel(name = "软件版本号", width = 15)
    @ApiModelProperty(value = "软件版本号")
    private java.lang.String sver;
	/**无功功率（W 瓦）*/
	@Excel(name = "无功功率（W 瓦）", width = 15)
    @ApiModelProperty(value = "无功功率（W 瓦）")
    private java.math.BigDecimal reactp;
	/**频率（Hz 赫兹）*/
	@Excel(name = "频率（Hz 赫兹）", width = 15)
    @ApiModelProperty(value = "频率（Hz 赫兹）")
    private java.math.BigDecimal freq;
	/**温度（摄氏度）*/
	@Excel(name = "温度（摄氏度）", width = 15)
    @ApiModelProperty(value = "温度（摄氏度）")
    private java.math.BigDecimal temper;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**时间戳*/
	@Excel(name = "时间戳", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时间戳")
    private java.util.Date tmstamp;
}
