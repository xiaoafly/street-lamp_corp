package org.jeecg.modules.corp.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.Channel;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.corp.entity.Lamp;
import org.jeecg.modules.corp.mapper.LampMapper;
import org.jeecg.modules.corp.netty.NettyChannelContainer;
import org.jeecg.modules.corp.netty.cloudbox.ControlPkg;
import org.jeecg.modules.corp.netty.lamp.LampFrameContent;
import org.jeecg.modules.corp.netty.lamp.LampQueryComm;
import org.jeecg.modules.corp.service.ILampService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: tb_lamp
 * @Author: jeecg-boot
 * @Date:   2022-09-08
 * @Version: V1.0
 */
@Service
public class LampServiceImpl extends ServiceImpl<LampMapper, Lamp> implements ILampService {

    @Override
    public void sendControlPackage(String id, List<Number> contentList) throws JsonProcessingException {
        Lamp lamp = getById(id);
        if(lamp==null){
            return;
        }
        String imei = lamp.getImsi();
        if(StringUtils.isBlank(imei)){
            return;
        }
        Channel channel = NettyChannelContainer.get(imei);
        if(channel==null||!channel.isActive()){
            return;
        }

        LampQueryComm comm = new LampQueryComm();
        comm.setMagic(LampFrameContent.CTRL_ON_MAGIC);
        comm.setDeviceAddr(Integer.parseInt(lamp.getCode()));

        comm.setContentList(contentList);

        channel.writeAndFlush(comm);
    }
}
