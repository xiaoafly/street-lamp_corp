package org.jeecg.modules.corp.netty;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.corp.entity.Lamp;
import org.jeecg.modules.corp.entity.LampData;
import org.jeecg.modules.corp.mapper.LampMapper;
import org.jeecg.modules.corp.netty.CloudBoxPackageContainer;
import org.jeecg.modules.corp.netty.lamp.LampAA55FramePkg;
import org.jeecg.modules.corp.netty.lamp.LampFrameContent;
import org.jeecg.modules.corp.netty.lamp.LampQueryComm;
import org.jeecg.modules.corp.netty.lamp.LampQueryOneResp;
import org.jeecg.modules.corp.service.IDeviceService;
import org.jeecg.modules.corp.service.ILampDataService;
import org.jeecg.modules.corp.service.ILampService;
import org.jeecg.modules.corp.service.IWeatherDataService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class LampConsumeTask {

    @Resource
    private ILampService lampService;
    @Resource
    private ILampDataService lampDataService;

    @Scheduled(fixedDelay = 1000)
    public void consumePackage(){
        boolean flag =true;
        while (flag){
            Object pkg = LampPackageContainer.getPackage();
            if(pkg ==null){
                flag = false;
                break;
            }
            if(pkg instanceof LampQueryOneResp){
                LampQueryOneResp qComm = (LampQueryOneResp) pkg;
                execQueryOnePackage(qComm);
            }
        }

    }



    /** 处理单灯采集信息指令响应数据包*/
    private void execQueryOnePackage(LampQueryOneResp pkg) {
        String imeiStr = pkg.getImeiStr();
        if(StringUtils.isBlank(imeiStr)){
            return;
        }
        QueryWrapper<Lamp> query = new QueryWrapper<>();
        query.eq("imsi",imeiStr);
        query.last(" limit 1");
        Lamp lamp = lampService.getOne(query);

        if(lamp!=null){
            Date now = new Date();
            LampData data = new LampData();
            data.setLampId(lamp.getId());
            if(pkg.getTgz()!=null){
                data.setBri(pkg.getTgz().intValue());
                lamp.setBri(data.getBri());
            }
            if(pkg.getDeviceAddr()!=null){
                lamp.setCode(pkg.getDeviceAddr().toString());
            }
            if(pkg.getKgzt()!=null){
                int kgzt = pkg.getKgzt().intValue();
                String onStatus = kgzt==0?"1":"0";
                data.setOnStatus(onStatus);
                lamp.setOnStatus(onStatus);

            }
            if(pkg.getDyz()!=null){
                BigDecimal dyz = new BigDecimal(pkg.getDyz());
                data.setVolt(dyz.divide(new BigDecimal(100),3, RoundingMode.HALF_UP));
                lamp.setVolt(data.getVolt());
            }
            if(pkg.getDlz()!=null){
                BigDecimal dlz = new BigDecimal(pkg.getDlz());
                data.setCurr(dlz.divide(new BigDecimal(1000),3, RoundingMode.HALF_UP));
                lamp.setCurr(data.getCurr());
            }
            if(pkg.getYgglz()!=null){
                BigDecimal ygglz = new BigDecimal(pkg.getYgglz());
                data.setActp(ygglz.divide(new BigDecimal(10),3, RoundingMode.HALF_UP));
                lamp.setActp(data.getActp());
            }
            if(pkg.getWgglz()!=null){
                BigDecimal wgglz = new BigDecimal(pkg.getWgglz());
                data.setReactp(wgglz.divide(new BigDecimal(10),3, RoundingMode.HALF_UP));
            }
            if(pkg.getPlz()!=null){
                BigDecimal plz = new BigDecimal(pkg.getPlz());
                data.setFreq(plz.divide(new BigDecimal(100),3, RoundingMode.HALF_UP));
            }
            if(pkg.getWdz()!=null){
                BigDecimal wdz = new BigDecimal(pkg.getWdz());
                data.setTemper(wdz.subtract(new BigDecimal(10000)).divide(new BigDecimal(100),3, RoundingMode.HALF_UP));
            }
            data.setImsi(imeiStr);
            data.setCreateTime(now);
            lamp.setUpdateTime(now);
            lamp.setOlStatus(LampFrameContent.online);
            lampDataService.save(data);
            lampService.updateById(lamp);

        }

    }

    @Scheduled(initialDelay=60000,fixedDelay = 600000)
    public void testOnline(){
        QueryWrapper<Lamp> query = new QueryWrapper<>();
        query.ne("status","0");
        query.eq("ol_status","1");
        query.leSql("update_time"," ADDDATE(now(),INTERVAL -10 MINUTE) ");
        List<Lamp> lampList = lampService.list(query);
        if(lampList==null||lampList.isEmpty()){
            return;
        }
        lampList.forEach(lamp -> {
            lamp.setOlStatus(LampFrameContent.offline);
        });
        lampService.updateBatchById(lampList);
    }
}
