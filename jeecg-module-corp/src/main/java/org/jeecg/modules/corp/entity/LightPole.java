package org.jeecg.modules.corp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 灯杆管理
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
@Data
@TableName("tb_light_pole")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_light_pole对象", description="灯杆管理")
public class LightPole implements Serializable {
    private static final long serialVersionUID = 1L;

	/**灯杆id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "灯杆id")
    private java.lang.String id;
	/**灯杆名称*/
	@Excel(name = "灯杆名称", width = 15)
    @ApiModelProperty(value = "灯杆名称")
    private java.lang.String lightName;
	/**灯杆编号*/
	@Excel(name = "灯杆编号", width = 15)
    @ApiModelProperty(value = "灯杆编号")
    private java.lang.String lightCode;
	/**经度*/
	@Excel(name = "经度", width = 15)
    @ApiModelProperty(value = "经度")
    private java.lang.String gwlongitude;
	/**纬度*/
	@Excel(name = "纬度", width = 15)
    @ApiModelProperty(value = "纬度")
    private java.lang.String gwlatitude;
	/**云盒zigbee 中心地址*/
	@Excel(name = "云盒zigbee 中心地址", width = 15)
    @ApiModelProperty(value = "云盒zigbee 中心地址")
    private java.lang.String boxId;
	/**云盒类型*/
	@Excel(name = "云盒类型", width = 15)
    @ApiModelProperty(value = "云盒类型")
    private java.lang.String boxType;
	/**云盒mac地址*/
	@Excel(name = "云盒mac地址", width = 15)
    @ApiModelProperty(value = "云盒mac地址")
    private java.lang.String boxMac;
	/**在线状态:0.掉线; 1.在线*/
	@Excel(name = "在线状态:0.掉线; 1.在线", width = 15)
    @ApiModelProperty(value = "在线状态:0.掉线; 1.在线")
    @Dict(dicCode = "ol_status")
    private java.lang.String olStatus;
	/**设备状态: 0.禁用; 1. 启用; 2.报警*/
	@Excel(name = "设备状态: 0.禁用; 1. 启用; 2.报警", width = 15)
    @ApiModelProperty(value = "设备状态: 0.禁用; 1. 启用; 2.报警")
    @Dict(dicCode = "device_status")
    private java.lang.String status;
	/**所属区域*/
	@Excel(name = "所属区域", width = 15)
    @ApiModelProperty(value = "所属区域")
    private java.lang.String areaId;
	/**所属分组id*/
	@Excel(name = "所属分组id", width = 15)
    @ApiModelProperty(value = "所属分组id")
    private java.lang.String groupId;
	/**所属项目id*/
	@Excel(name = "所属项目id", width = 15)
    @ApiModelProperty(value = "所属项目id")
    private java.lang.String projectId;
	/**创建人(用户名)*/
    @ApiModelProperty(value = "创建人(用户名)")
    private java.lang.String createBy;
	/**创建时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新人(用户名)*/
    @ApiModelProperty(value = "更新人(用户名)")
    private java.lang.String updateBy;
	/**更新时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**时间戳*/
	@Excel(name = "时间戳", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时间戳")
    private java.util.Date tmstamp;
}
