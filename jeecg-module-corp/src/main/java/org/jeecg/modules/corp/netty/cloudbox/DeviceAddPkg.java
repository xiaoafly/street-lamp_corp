package org.jeecg.modules.corp.netty.cloudbox;

import lombok.Data;

import java.util.List;

/**
 * 添加设备指令
 */
@Data
public class DeviceAddPkg extends BasePkg {

    private List<Sladdr> sladdrs;
    private Integer control;


}
