package org.jeecg.modules.corp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 气象设备
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
@Data
@TableName("tb_device")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_device对象", description="气象设备")
public class Device implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**所属灯杆id*/
	@Excel(name = "所属灯杆id", width = 15, dictTable = "tb_light_pole", dicText = "light_name", dicCode = "id")
    @Dict(dictTable = "tb_light_pole", dicText = "light_name", dicCode = "id")
    @ApiModelProperty(value = "所属灯杆id")
    private java.lang.String lightPoleId;
	/**设备名称*/
	@Excel(name = "设备名称", width = 15)
    @ApiModelProperty(value = "设备名称")
    private java.lang.String name;
	/**设备id*/
	@Excel(name = "设备id", width = 15)
    @ApiModelProperty(value = "设备id")
    private java.lang.String deviceId;
	/**设备类型:1.照明; 2.回路设备;3.工业设备;4.气象*/
	@Excel(name = "设备类型:1.照明; 2.回路设备;3.工业设备;4.气象", width = 15)
    @Dict(dicCode = "device_type")
    @ApiModelProperty(value = "设备类型:1.照明; 2.回路设备;3.工业设备;4.气象")
    private java.lang.String deviceType;
	/**设备型号*/
	@Excel(name = "设备型号", width = 15)
    @ApiModelProperty(value = "设备型号")
    private java.lang.String deviceModel;
	/**设备编号*/
	@Excel(name = "设备编号", width = 15)
    @ApiModelProperty(value = "设备编号")
    private java.lang.String deviceCode;
	/**端口号*/
	@Excel(name = "端口号", width = 15)
    @ApiModelProperty(value = "端口号")
    private java.lang.Integer ep;
	/**所属项目id*/
	@Excel(name = "所属项目id", width = 15)
    @ApiModelProperty(value = "所属项目id")
    @Dict(dictTable = "tb_project", dicText = "project_name", dicCode = "id")
    private java.lang.String projectId;
	/**所属分组id*/
	@Excel(name = "所属分组id", width = 15)
    @ApiModelProperty(value = "所属分组id")
    @Dict(dictTable = "tb_group", dicText = "group_name", dicCode = "id")
    private java.lang.String groupId;
	/**在线状态:0.掉线; 1.在线*/
	@Excel(name = "在线状态:0.掉线; 1.在线", width = 15)
    @ApiModelProperty(value = "在线状态:0.掉线; 1.在线")
    @Dict(dicCode = "ol_status")
    private java.lang.String olStatus;
	/**设备状态: 0.禁用; 1. 启用; 2.报警*/
	@Excel(name = "设备状态: 0.禁用; 1. 启用; 2.报警", width = 15)
    @ApiModelProperty(value = "设备状态: 0.禁用; 1. 启用; 2.报警")
    @Dict(dicCode = "device_status")
    private java.lang.String status;
	/**创建人(用户名)*/
    @ApiModelProperty(value = "创建人(用户名)")
    private java.lang.String createBy;
	/**创建时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新人(用户名)*/
    @ApiModelProperty(value = "更新人(用户名)")
    private java.lang.String updateBy;
	/**更新时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**时间戳*/
	@Excel(name = "时间戳", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时间戳")
    private java.util.Date tmstamp;
}
