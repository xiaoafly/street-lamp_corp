package org.jeecg.modules.corp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.corp.entity.LightPole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 灯杆管理
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
public interface LightPoleMapper extends BaseMapper<LightPole> {

}
