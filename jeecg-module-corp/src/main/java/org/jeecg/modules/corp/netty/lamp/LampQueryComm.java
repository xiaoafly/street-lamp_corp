package org.jeecg.modules.corp.netty.lamp;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class LampQueryComm {
    private byte magic;
    private int deviceAddr;
    private List<Number> contentList;


}
