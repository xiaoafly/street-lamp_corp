package org.jeecg.modules.corp.netty.cloudbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DevicePkg {
    /** 设备id*/
    private String id;
    /** 端口号*/
    private Integer ep;
    private Boolean ol;
    private String dn;
    private Integer dtype;
    private String fac;
    private Integer ztype;
    private String dsp;
    private String swid;

    private Map<String,Object> st;


}
