package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.LampData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: tb_lamp_data
 * @Author: jeecg-boot
 * @Date:   2022-11-04
 * @Version: V1.0
 */
public interface ILampDataService extends IService<LampData> {

}
