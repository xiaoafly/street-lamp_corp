package org.jeecg.modules.corp.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.corp.constant.DeviceConstant;
import org.jeecg.modules.corp.entity.Device;
import org.jeecg.modules.corp.service.IDeviceService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 气象管理
 * @Author: jeecg-boot
 * @Date:   2022-11-09
 * @Version: V1.0
 */
@Api(tags="气象管理")
@RestController
@RequestMapping("/corp/weather")
@Slf4j
public class WeatherController extends JeecgController<Device, IDeviceService> {
	@Autowired
	private IDeviceService deviceService;

	
	/**
	 * 分页列表查询
	 *
	 * @param weather
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "气象-分页列表查询")
	@ApiOperation(value="气象-分页列表查询", notes="气象-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Device>> queryPageList(Device weather,
											   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
											   HttpServletRequest req) {
		QueryWrapper<Device> queryWrapper = QueryGenerator.initQueryWrapper(weather, req.getParameterMap());
		queryWrapper.eq("device_type",DeviceConstant.DEVICE_TYPE_WEATHER);
		Page<Device> page = new Page<Device>(pageNo, pageSize);
		IPage<Device> pageList = deviceService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param weather
	 * @return
	 */
	@AutoLog(value = "气象-添加")
	@ApiOperation(value="气象-添加", notes="气象-添加")
	//@RequiresPermissions("org.jeecg.modules:tb_weather:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Device weather) {
		weather.setDeviceType(DeviceConstant.DEVICE_TYPE_WEATHER);
		deviceService.save(weather);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param weather
	 * @return
	 */
	@AutoLog(value = "气象-编辑")
	@ApiOperation(value="气象-编辑", notes="气象-编辑")
	//@RequiresPermissions("org.jeecg.modules:tb_weather:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Device weather) {
		deviceService.updateById(weather);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "气象-通过id删除")
	@ApiOperation(value="气象-通过id删除", notes="气象-通过id删除")
	//@RequiresPermissions("org.jeecg.modules:tb_weather:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		deviceService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "气象-批量删除")
	@ApiOperation(value="气象-批量删除", notes="气象-批量删除")
	//@RequiresPermissions("org.jeecg.modules:tb_weather:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.deviceService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "气象-通过id查询")
	@ApiOperation(value="气象-通过id查询", notes="气象-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Device> queryById(@RequestParam(name="id",required=true) String id) {
		Device weather = deviceService.getById(id);
		if(weather==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(weather);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param weather
    */
    //@RequiresPermissions("org.jeecg.modules:tb_weather:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Device weather) {
        return super.exportXls(request, weather, Device.class, "气象");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_weather:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Device.class);
    }

}
