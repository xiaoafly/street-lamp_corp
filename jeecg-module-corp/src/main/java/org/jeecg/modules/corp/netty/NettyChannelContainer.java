package org.jeecg.modules.corp.netty;

import io.netty.channel.Channel;
import io.netty.channel.socket.SocketChannel;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 存放连接的channel对象
 */

public class NettyChannelContainer {

    private static final Map<String, Channel> map = new ConcurrentHashMap<String, Channel>();


    public static void add(String clientId, Channel channel) {
        map.put(clientId, channel);
    }

    public static Channel get(String clientId) {
        return map.get(clientId);
    }

    public static void remove(Channel socketChannel) {
        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue() == socketChannel) {
                map.remove(entry.getKey());
            }
        }
    }

    public static boolean containValue(Channel socketChannel) {
        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue() == socketChannel) {
                return true;
            }
        }
        return false;
    }

    public static boolean containKey(String clientId) {
        if(StringUtils.isBlank(clientId)){
            return false;
        }
        return map.containsKey(clientId);

    }


}