package org.jeecg.modules.corp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.corp.constant.DeviceConstant;
import org.jeecg.modules.corp.entity.Camera;
import org.jeecg.modules.corp.entity.Device;
import org.jeecg.modules.corp.service.IDeviceService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.corp.util.EzvizUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 监控管理
 * @Author: jeecg-boot
 * @Date:   2022-11-09
 * @Version: V1.0
 */
@Api(tags="气象管理")
@RestController
@RequestMapping("/corp/cam")
@Slf4j
public class CameraController extends JeecgController<Device, IDeviceService> {
    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private EzvizUtils ezvizUtils;


    /**
     * 分页列表查询
     *
     * @param device
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    //@AutoLog(value = "气象-分页列表查询")
    @ApiOperation(value="监控-分页列表查询", notes="监控-分页列表查询")
    @GetMapping(value = "/list")
    public Result<IPage<Device>> queryPageList(Device device,
                                               @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                               @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                               HttpServletRequest req) {
        QueryWrapper<Device> queryWrapper = QueryGenerator.initQueryWrapper(device, req.getParameterMap());
        queryWrapper.eq("device_type", DeviceConstant.DEVICE_TYPE_CAMERA);
        Page<Device> page = new Page<Device>(pageNo, pageSize);
        IPage pageList = deviceService.page(page, queryWrapper);
        if(pageList!=null||pageList.getSize()>0){
            List<Device> deviceList = pageList.getRecords();
            List<Camera> cameraList = new ArrayList<>();
            deviceList.forEach(d->{
                Camera cam = new Camera();
                BeanUtils.copyProperties(d,cam);
                String accessToken = ezvizUtils.getAccessToken();
                String online = ezvizUtils.getDeviceInfo(accessToken,d.getDeviceId());
                String url = ezvizUtils.getVideoAddress(accessToken,d.getDeviceId());
                cam.setOlStatus(online);
                cam.setUrl(url);
                cam.setAccessToken(accessToken);

                cameraList.add(cam);
            });
            pageList.setRecords(cameraList);
        }

        return Result.OK(pageList);
    }

    /**
     *   添加
     *
     * @param device
     * @return
     */
    @AutoLog(value = "监控-添加")
    @ApiOperation(value="监控-添加", notes="监控-添加")
    //@RequiresPermissions("org.jeecg.modules:tb_weather:add")
    @PostMapping(value = "/add")
    public Result<String> add(@RequestBody Device device) {
        device.setDeviceType(DeviceConstant.DEVICE_TYPE_CAMERA);
        deviceService.save(device);
        return Result.OK("添加成功！");
    }

    /**
     *  编辑
     *
     * @param device
     * @return
     */
    //@AutoLog(value = "智慧监控-分页列表查询")
    @ApiOperation(value="监控-编辑", notes="监控-编辑")
    //@RequiresPermissions("org.jeecg.modules:tb_weather:edit")
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
    public Result<String> edit(@RequestBody Device device) {
        deviceService.updateById(device);
        return Result.OK("编辑成功!");
    }

    /**
     *   通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "监控-通过id删除")
    @ApiOperation(value="监控-通过id删除", notes="监控-通过id删除")
    //@RequiresPermissions("org.jeecg.modules:tb_weather:delete")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam(name="id",required=true) String id) {
        deviceService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     *  批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "监控-批量删除")
    @ApiOperation(value="监控-批量删除", notes="监控-批量删除")
    //@RequiresPermissions("org.jeecg.modules:tb_weather:deleteBatch")
    @DeleteMapping(value = "/deleteBatch")
    public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.deviceService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    //@AutoLog(value = "智慧监控-通过id查询")
    @ApiOperation(value="监控-通过id查询", notes="监控-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<Device> queryById(@RequestParam(name="id",required=true) String id) {
        Device device = deviceService.getById(id);
        if(device==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(device);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param device
     */
    //@RequiresPermissions("org.jeecg.modules:tb_weather:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Device device) {
        return super.exportXls(request, device, Device.class, "气象");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    //@RequiresPermissions("tb_weather:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Device.class);
    }

}
