package org.jeecg.modules.corp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.corp.entity.Device;
import org.jeecg.modules.corp.mapper.DeviceMapper;
import org.jeecg.modules.corp.service.IDeviceService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 气象设备
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
@Service
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements IDeviceService {

    @Override
    public Device findOneByDeviceId(String deviceId) {
        if(StringUtils.isBlank(deviceId)){
            return null;
        }
        QueryWrapper<Device> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_id",deviceId);
        List<Device> deviceList =  list(queryWrapper);
        if(deviceList==null||deviceList.size()<1){
            return null;
        }
        return  deviceList.get(0);
    }
}
