package org.jeecg.modules.corp.netty.cloudbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GW {
    private String gwver;
    private String ver;
    private String clientver;
    private String chiptype;
    private String HW_model;
    private Integer grssi4;
    private String gwapptype;
    private Integer gwtype;
    private Integer gwmode;
    private String id;
    private String mac;


}
