package org.jeecg.modules.corp.netty.lamp;

import lombok.Data;

/**
 * 查询单灯采集信息指令响应包
 */
@Data
public class LampQueryResp {
    private Short magic;
    private Short playloadlen;
    private Long deviceAddr;

    private Integer crc16;



}
