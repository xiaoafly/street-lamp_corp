package org.jeecg.modules.corp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.corp.entity.WeatherData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 气象历史数据
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
public interface WeatherDataMapper extends BaseMapper<WeatherData> {

}
