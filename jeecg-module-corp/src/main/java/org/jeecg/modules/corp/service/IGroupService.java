package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.Group;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 设备分组
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
public interface IGroupService extends IService<Group> {

}
