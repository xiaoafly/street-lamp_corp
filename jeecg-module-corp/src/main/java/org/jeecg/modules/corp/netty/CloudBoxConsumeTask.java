package org.jeecg.modules.corp.netty;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.jeecg.modules.corp.constant.DeviceConstant;
import org.jeecg.modules.corp.entity.Device;
import org.jeecg.modules.corp.entity.WeatherData;
import org.jeecg.modules.corp.mapper.LampMapper;
import org.jeecg.modules.corp.netty.cloudbox.*;
import org.jeecg.modules.corp.service.IDeviceService;
import org.jeecg.modules.corp.service.IWeatherDataService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class CloudBoxConsumeTask {

    @Resource
    private LampMapper lampMapper;
    @Resource
    private IDeviceService deviceService;
    @Resource
    private IWeatherDataService weatherDataService;

    @Scheduled(fixedDelay = 30000)
    public void consumePackage(){
        boolean flag =true;
        while (flag){
            BasePkg basePkg = CloudBoxPackageContainer.getPackage();
            if(basePkg ==null){
                flag = false;
                break;
            }
            if(basePkg instanceof HeartPkg){
                HeartPkg heart = (HeartPkg) basePkg;
                execHeartPackage(heart);
            }else if(basePkg instanceof StatusChangePkg){
                StatusChangePkg statusChange = (StatusChangePkg) basePkg;
                execStatusChange(statusChange);
            }
        }

    }

    /**处理状态改变包*/
    private void execStatusChange(StatusChangePkg statusChange) {
        String deviceId = statusChange.getId();
        Device device = deviceService.findOneByDeviceId(deviceId);
        if(device==null){
            return;
        }
        if(statusChange.getEp()!=null){
            device.setEp(statusChange.getEp());
        }
        if(statusChange.getOl()!=null){
            device.setOlStatus(statusChange.getOl()?"1":"0");
        }
        Date updateTime = new Date();
        device.setUpdateTime(updateTime);
        deviceService.updateById(device);
        Map<String,Object> st = statusChange.getSt();
        if(st==null||st.isEmpty()){
            return;
        }
        if(DeviceConstant.DEVICE_TYPE_WEATHER.equals(device.getDeviceType())){
            WeatherData wData = new WeatherData();
            wData.setDeviceId(device.getDeviceId());
            try {
                BeanUtils.populate(wData,st);
                if(updateTime!=null){
                    wData.setCreateTime(updateTime);
                }
                weatherDataService.save(wData);
            } catch (Exception e) {
                log.error(e.getMessage(),e);
            }
        }
    }

    /** 处理心跳包*/
    private void execHeartPackage(HeartPkg heart) {
        List<DevicePkg> devicePkgList = heart.getDevice();
        if(devicePkgList ==null|| devicePkgList.isEmpty()){
            return;
        }
        Long timestamp = heart.getTimestamp();
        if(timestamp==null){
            log.warn("心跳包使用generate_time的时间{}",heart.getGenerateTime());
            timestamp = heart.getGenerateTime();
        }
        Date updateTime = null;
        if(timestamp!=null){
            updateTime = new Date(timestamp *1000);
        }
        GW gw = heart.getGw();
        if(gw==null){
            gw = new GW();
        }
        for (DevicePkg dpkg:devicePkgList) {
            Device device = deviceService.findOneByDeviceId(dpkg.getId());
            if(device==null){
                continue;
            }
            if(dpkg.getEp()!=null) {
                device.setEp(dpkg.getEp());
            }
            if(dpkg.getOl()!=null){
                device.setOlStatus(dpkg.getOl()?"1":"0");
            }
            if(updateTime!=null){
                device.setUpdateTime(updateTime);
            }
            deviceService.updateById(device);
            Map<String,Object> st = dpkg.getSt();
            if(st==null||st.isEmpty()){
                continue;
            }
            if(DeviceConstant.DEVICE_TYPE_WEATHER.equals(device.getDeviceType())){
                WeatherData wData = new WeatherData();
                wData.setDeviceId(device.getDeviceId());
                try {
                    BeanUtils.populate(wData,st);
                    if(updateTime!=null){
                        wData.setCreateTime(updateTime);
                    }
                    weatherDataService.save(wData);
                } catch (Exception e) {
                   log.error(e.getMessage(),e);
                }
            }
        }

    }
}
