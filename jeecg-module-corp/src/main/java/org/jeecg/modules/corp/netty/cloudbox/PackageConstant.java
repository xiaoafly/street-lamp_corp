package org.jeecg.modules.corp.netty.cloudbox;

public class PackageConstant {

    /**包code**/
    /**心跳*/
    public static final int HEART_CODE = 101;
    /**心跳_响应*/
    public static final int HEART_RESP_CODE = 10101;
    /**控制包*/
    public static final int CONTROL_CODE = 102;
    /**控制包*/
    public static final int CONTROL_RESP_CODE = 1002;
    /** 添加设备*/
    public static final int DEVICE_ADD = 1010;
    /** 删除设备*/
    public static final int DEVICE_DEL = 1003;
    /**设备状态改变*/
    public static final int DEVICE_CHANGE = 104;
    /**设备状态改变_响应*/
    public static final int DEVICE_CHANGE_RESP = 1004;

}
