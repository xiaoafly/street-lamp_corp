package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.News;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 咨询
 * @Author: jeecg-boot
 * @Date:   2022-10-21
 * @Version: V1.0
 */
public interface INewsService extends IService<News> {

}
