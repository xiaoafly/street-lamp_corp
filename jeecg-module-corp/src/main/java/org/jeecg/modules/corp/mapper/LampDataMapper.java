package org.jeecg.modules.corp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.corp.entity.LampData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: tb_lamp_data
 * @Author: jeecg-boot
 * @Date:   2022-11-04
 * @Version: V1.0
 */
public interface LampDataMapper extends BaseMapper<LampData> {

}
