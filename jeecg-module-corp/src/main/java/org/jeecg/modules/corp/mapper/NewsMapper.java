package org.jeecg.modules.corp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.corp.entity.News;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 咨询
 * @Author: jeecg-boot
 * @Date:   2022-10-21
 * @Version: V1.0
 */
public interface NewsMapper extends BaseMapper<News> {

}
