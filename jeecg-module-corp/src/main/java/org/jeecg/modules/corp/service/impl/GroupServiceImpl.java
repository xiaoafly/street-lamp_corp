package org.jeecg.modules.corp.service.impl;

import org.jeecg.modules.corp.entity.Group;
import org.jeecg.modules.corp.mapper.GroupMapper;
import org.jeecg.modules.corp.service.IGroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 设备分组
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements IGroupService {

}
