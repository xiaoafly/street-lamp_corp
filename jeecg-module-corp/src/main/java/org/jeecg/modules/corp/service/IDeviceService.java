package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.Device;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 气象设备
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
public interface IDeviceService extends IService<Device> {

    public Device findOneByDeviceId(String deviceId);
}
