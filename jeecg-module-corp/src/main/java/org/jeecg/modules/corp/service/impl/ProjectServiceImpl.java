package org.jeecg.modules.corp.service.impl;

import org.jeecg.modules.corp.entity.Project;
import org.jeecg.modules.corp.mapper.ProjectMapper;
import org.jeecg.modules.corp.service.IProjectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目信息
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements IProjectService {

}
