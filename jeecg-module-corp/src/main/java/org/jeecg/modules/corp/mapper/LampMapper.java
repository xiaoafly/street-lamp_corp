package org.jeecg.modules.corp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.corp.entity.Lamp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: tb_lamp
 * @Author: jeecg-boot
 * @Date:   2022-09-08
 * @Version: V1.0
 */
public interface LampMapper extends BaseMapper<Lamp> {

     int updateHeartByPrimaryKey(Lamp lamp);
}
