package org.jeecg.modules.corp.netty;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public class LampPackageContainer {
    private static final LinkedBlockingQueue<Object> packageQueue = new LinkedBlockingQueue<Object>(10000);

    public static void putPackage(Object pkg)  {
        try {
            packageQueue.put(pkg);
        } catch (InterruptedException e) {
            log.error(e.getMessage(),e);
        }
    }

    public static Object getPackage(){
        return packageQueue.poll();
    }
}
