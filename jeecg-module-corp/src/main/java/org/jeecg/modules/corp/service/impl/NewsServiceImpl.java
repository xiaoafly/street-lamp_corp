package org.jeecg.modules.corp.service.impl;

import org.jeecg.modules.corp.entity.News;
import org.jeecg.modules.corp.mapper.NewsMapper;
import org.jeecg.modules.corp.service.INewsService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 咨询
 * @Author: jeecg-boot
 * @Date:   2022-10-21
 * @Version: V1.0
 */
@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, News> implements INewsService {

}
