package org.jeecg.modules.corp.netty;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.corp.netty.cloudbox.BasePkg;

import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public class CloudBoxPackageContainer {
    private static final LinkedBlockingQueue<BasePkg> packageQueue = new LinkedBlockingQueue<BasePkg>(10000);

    public static void putPackage(BasePkg basePkg)  {
        try {
            packageQueue.put(basePkg);
        } catch (InterruptedException e) {
            log.error(e.getMessage(),e);
        }
    }

    public static BasePkg getPackage(){
        return packageQueue.poll();
    }
}
