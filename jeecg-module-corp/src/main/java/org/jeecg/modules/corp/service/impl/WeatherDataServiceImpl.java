package org.jeecg.modules.corp.service.impl;

import org.jeecg.modules.corp.entity.WeatherData;
import org.jeecg.modules.corp.mapper.WeatherDataMapper;
import org.jeecg.modules.corp.service.IWeatherDataService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 气象历史数据
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
@Service
public class WeatherDataServiceImpl extends ServiceImpl<WeatherDataMapper, WeatherData> implements IWeatherDataService {

}
