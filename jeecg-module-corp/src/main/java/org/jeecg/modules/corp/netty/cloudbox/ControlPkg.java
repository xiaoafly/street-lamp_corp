package org.jeecg.modules.corp.netty.cloudbox;

import lombok.Data;

import java.util.Map;

@Data
public class ControlPkg extends BasePkg {

    private String id;
    private Integer ep;
    private Map<String,Object> control;
}
