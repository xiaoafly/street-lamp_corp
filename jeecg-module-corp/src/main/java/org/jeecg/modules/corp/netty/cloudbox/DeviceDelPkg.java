package org.jeecg.modules.corp.netty.cloudbox;

import lombok.Data;

/**
 * 删除设备指令(响应)
 */
@Data
public class DeviceDelPkg extends BasePkg {

    private String id;
    private Integer ep;
    private Integer result;


}
