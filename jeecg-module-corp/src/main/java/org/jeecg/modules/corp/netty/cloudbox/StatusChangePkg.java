package org.jeecg.modules.corp.netty.cloudbox;

import lombok.Data;

import java.util.Map;

@Data
public class StatusChangePkg extends BasePkg{
    private String  id;
    private Integer ep;
    private Integer pid;
    private Integer did;
    private Integer control;
    private Boolean ol;
    private Map<String,Object> st;


}
