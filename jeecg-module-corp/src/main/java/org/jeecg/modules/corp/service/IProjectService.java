package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.Project;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目信息
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
public interface IProjectService extends IService<Project> {

}
