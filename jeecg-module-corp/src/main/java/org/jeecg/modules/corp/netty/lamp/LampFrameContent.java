package org.jeecg.modules.corp.netty.lamp;

import io.netty.util.AttributeKey;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class LampFrameContent {

    public static final AttributeKey<String> IMEI_ATTR =  AttributeKey.valueOf("IMEI");

    /***
     *  帧头
     */
    /**AA55指令*/
    public static final int AA55_MAGIC = 0xAA55;//43605
    /**查询单灯采集信息指令*/
    public static final byte QUERY_ONE_MAGIC = 0x10;
    public static final byte QUERY_ONE_MAGIC_RESP = 0x11;

    /**灯控器开关、调光指令*/
    public static final byte CTRL_ON_MAGIC = 0x15;



    public static final Charset CHARSET = StandardCharsets.UTF_8;

    /***
     *  功能码
     */
    /** 注册包、心跳包功能码*/
    public static final short REG_FUN_CODE = 0xD0;


    /***
     *  固定值
     */

    /** 调光端口*/
    public static final byte TGDK  = 0x04;
    /** 调光功能码*/
    public static final byte TGGNM  = 0x10;
    /** 开关端口*/
    public static final byte KGDK  = 0x05;
    /** 开关功能码*/
    public static final byte KGGNM  = 0x04;



    public static final String IMEI ="IMEI";
    public static final String IMSI ="IMSI";
    public static final String CSQ ="CSQ";
    public static final String PCI ="PCI";


    public static final String online ="1";
    public static final String offline="0";







}
