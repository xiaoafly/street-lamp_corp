package org.jeecg.modules.corp.service.impl;

import org.jeecg.modules.corp.entity.LampData;
import org.jeecg.modules.corp.mapper.LampDataMapper;
import org.jeecg.modules.corp.service.ILampDataService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_lamp_data
 * @Author: jeecg-boot
 * @Date:   2022-11-04
 * @Version: V1.0
 */
@Service
public class LampDataServiceImpl extends ServiceImpl<LampDataMapper, LampData> implements ILampDataService {

}
