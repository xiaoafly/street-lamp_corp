package org.jeecg.modules.corp.netty.cloudbox;

import lombok.Data;

import java.util.List;

@Data
public class HeartPkg extends BasePkg {

    private Long timestamp;
    private Long generateTime;
    private GW gw;
    private List<DevicePkg> device;
}
