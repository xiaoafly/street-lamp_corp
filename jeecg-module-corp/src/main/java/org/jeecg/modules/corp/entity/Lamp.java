package org.jeecg.modules.corp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 照明管理
 * @Author: jeecg-boot
 * @Date:   2022-10-05
 * @Version: V1.0
 */
@Data
@TableName("tb_lamp")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_lamp对象", description="tb_lamp")
public class Lamp implements Serializable {
    private static final long serialVersionUID = 1L;

    /**路灯设备ID*/
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "路灯设备ID")
    private java.lang.String id;
    /**名称*/
    @Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
    /**灯控器地址*/
    @Excel(name = "灯控器地址", width = 15)
    @ApiModelProperty(value = "灯控器地址")
    private java.lang.String code;
    /**端口号*/
    @Excel(name = "端口号", width = 15)
    @ApiModelProperty(value = "端口号")
    private java.lang.Integer ep;
    /**协议*/
    @Excel(name = "协议", width = 15)
    @ApiModelProperty(value = "协议")
    private java.lang.Integer pid;
    /**设备类型 id*/
    @Excel(name = "设备类型 id", width = 15, dicCode = "device_type")
    @Dict(dicCode = "device_type")
    @ApiModelProperty(value = "设备类型 id")
    private java.lang.Integer did;
    /**当前亮度状态: 1-254*/
    @Excel(name = "当前亮度状态: 1-254", width = 15)
    @ApiModelProperty(value = "当前亮度状态: 1-254")
    private java.lang.Integer bri;
    /**色温: 0-100*/
    @Excel(name = "色温: 0-100", width = 15)
    @ApiModelProperty(value = "色温: 0-100")
    private java.lang.Integer colorTemp;
    /**实时电压*/
    @Excel(name = "实时电压", width = 15)
    @ApiModelProperty(value = "实时电压")
    private java.math.BigDecimal volt;
    /**实时电流*/
    @Excel(name = "实时电流", width = 15)
    @ApiModelProperty(value = "实时电流")
    private java.math.BigDecimal curr;
    /**有功功率*/
    @Excel(name = "有功功率", width = 15)
    @ApiModelProperty(value = "有功功率")
    private java.math.BigDecimal actp;
    /**有功电能*/
    @Excel(name = "有功电能", width = 15)
    @ApiModelProperty(value = "有功电能")
    private java.math.BigDecimal acte;
    /**运行时间（分）*/
    @Excel(name = "运行时间（分）", width = 15)
    @ApiModelProperty(value = "运行时间（分）")
    private java.lang.String lightrt;
    /**经度*/
    @Excel(name = "经度", width = 15)
    @ApiModelProperty(value = "经度")
    private java.lang.String gwlongitude;
    /**纬度*/
    @Excel(name = "纬度", width = 15)
    @ApiModelProperty(value = "纬度")
    private java.lang.String gwlatitude;
    /**设备信号强度：0-100*/
    @Excel(name = "设备信号强度：0-100", width = 15)
    @ApiModelProperty(value = "设备信号强度：0-100")
    private java.lang.Integer rssi;
    /**设备SIM卡标识号*/
    @Excel(name = "设备SIM卡标识号", width = 15)
    @ApiModelProperty(value = "设备SIM卡标识号")
    private java.lang.String imsi;
    /**所属项目id*/
    @Excel(name = "所属项目id", width = 15, dictTable = "tb_project", dicText = "project_name", dicCode = "id")
    @Dict(dictTable = "tb_project", dicText = "project_name", dicCode = "id")
    @ApiModelProperty(value = "所属项目id")
    private java.lang.String projectId;
    /**所属分组id*/
    @Excel(name = "所属分组id", width = 15, dictTable = "tb_group", dicText = "group_name", dicCode = "id")
    @Dict(dictTable = "tb_group", dicText = "group_name", dicCode = "id")
    @ApiModelProperty(value = "所属分组id")
    private java.lang.String groupId;
    /**所属基站*/
    @Excel(name = "所属基站", width = 15)
    @ApiModelProperty(value = "所属基站")
    private java.lang.String pci;
    /**在线状态:0.掉线; 1.在线*/
    @Excel(name = "在线状态:0.掉线; 1.在线", width = 15)
    @ApiModelProperty(value = "在线状态:0.掉线; 1.在线")
    @Dict(dicCode = "ol_status")
    private java.lang.String olStatus;
    /**开关状态：0.关; 1 开*/
    @Excel(name = "开关状态：0.关; 1 开", width = 15)
    @ApiModelProperty(value = "开关状态：0.关; 1 开")
    @Dict(dicCode = "is_open")
    private java.lang.String onStatus;
    /**设备状态: 0.禁用; 1. 启用; 2.报警*/
    @Excel(name = "设备状态: 0.禁用; 1. 启用; 2.报警", width = 15, dicCode = "device_status")
    @Dict(dicCode = "device_status")
    @ApiModelProperty(value = "设备状态: 0.禁用; 1. 启用; 2.报警")
    private java.lang.String status;
    /**灯具类型*/
    @Excel(name = "灯具类型", width = 15, dicCode = "lamp_type")
    @Dict(dicCode = "lamp_type")
    @ApiModelProperty(value = "灯具类型")
    private java.lang.String lampType;
    /**硬件版本号*/
    @Excel(name = "硬件版本号", width = 15)
    @ApiModelProperty(value = "硬件版本号")
    private java.lang.String hver;
    /**软件版本号*/
    @Excel(name = "软件版本号", width = 15)
    @ApiModelProperty(value = "软件版本号")
    private java.lang.String sver;
    /**创建人(用户名)*/
    @ApiModelProperty(value = "创建人(用户名)")
    private java.lang.String createBy;
    /**创建时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    /**更新人(用户名)*/
    @ApiModelProperty(value = "更新人(用户名)")
    private java.lang.String updateBy;
    /**更新时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
    /**时间戳*/
    @Excel(name = "时间戳", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时间戳")
    private java.util.Date tmstamp;

    /**所属云盒mac地址*/
    @Excel(name = "所属云盒mac地址", width = 15)
    @ApiModelProperty(value = "所属云盒mac地址")
    private java.lang.String mac;
}
