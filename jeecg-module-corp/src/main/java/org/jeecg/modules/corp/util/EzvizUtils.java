package org.jeecg.modules.corp.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 和萤石云对接工具
 */
@Component
@Slf4j
public class EzvizUtils {
    @Autowired
    RedisUtil redisUtil;

    @Value("${ezviz.appKey}")
    private String appKey;

    @Value("${ezviz.appSecret}")
    private String appSecret;

    private JSONObject paramsApp ;


    @PostConstruct
    public void init(){
        paramsApp = new JSONObject();
        paramsApp.put("appKey",appKey);
        paramsApp.put("appSecret",appSecret);
    }

    /** 获取accessToken的URL*/
    private static final String URL_ACCESSTOKEN ="https://open.ys7.com/api/lapp/token/get";
    /** 获取指定设备的信息的URL*/
    private static final String URL_DEVICEINFO ="https://open.ys7.com/api/lapp/device/info";
    /** 获取设备播放地址的URL*/
    private static final String URL_VIDEOADDRESS ="https://open.ys7.com/api/lapp/v2/live/address/get";
    private String accessToken= "at.7i166b6f7erv9lw46g9tgz504gggy86v-1sebuh9rvy-1r36u9y-0wjazl2bc";
    private static final String KEY_ACCESSTOKEN ="ezviz_accessToken";
    /**
     * 根据appKey和secret获取accessToken
     */
    public String getAccessToken(){
        String accessToken= null;
        Object value = redisUtil.get(KEY_ACCESSTOKEN);
        if(value!=null){
            accessToken = value.toString();
        }else {
            JSONObject jsonObject = RestUtil.post(URL_ACCESSTOKEN,paramsApp,null);
            String code = jsonObject.getString("code");
            if(!"200".equals(code)){
                String errMsg = jsonObject.getString("msg");
                log.error("请求萤石云AccessToken错误：{}",errMsg);
                return null;
            }
            JSONObject data = jsonObject.getJSONObject("data");
            accessToken = data.getString("accessToken");
            long expireTime = data.getLongValue("expireTime");
            long expire = expireTime - System.currentTimeMillis();
            redisUtil.set(KEY_ACCESSTOKEN,accessToken,expire);
        }
        return accessToken;
    }

    /**
     * 根据deviceId获取设备是否在线
     * @param deviceId
     * @return 在线状态：0-不在线，1-在线
     */
    public String getDeviceInfo(String accessToken,String deviceId){
        if(StringUtils.isEmpty(deviceId)){
            return "0";
        }
        deviceId = deviceId.toUpperCase();

        JSONObject paramsDevice = new JSONObject();
        paramsDevice.put("accessToken",accessToken);
        paramsDevice.put("deviceSerial",deviceId);
        JSONObject jsonObject = RestUtil.post(URL_DEVICEINFO,paramsDevice,null);
        String code = jsonObject.getString("code");
        if(!"200".equals(code)){
            String errMsg = jsonObject.getString("msg");
            log.error("请求萤石云DeviceInfo错误：{}",errMsg);
            if("10002".equals(code)){
                redisUtil.del(KEY_ACCESSTOKEN);
            }
            return "0";
        }
        JSONObject data = jsonObject.getJSONObject("data");
        String status = data.getString("status");
        return status;
    }



    /**
     * 根据accessToken和deviceId获取直播地址
     * @param accessToken
     * @param deviceId
     * @return
     */
    public String getVideoAddress(String accessToken,String deviceId){
        if(StringUtils.isEmpty(deviceId)||StringUtils.isEmpty(accessToken)){
            return null;
        }
        deviceId = deviceId.toUpperCase();

        JSONObject paramsDevice = new JSONObject();
        paramsDevice.put("accessToken",accessToken);
        paramsDevice.put("deviceSerial",deviceId);
        JSONObject jsonObject = RestUtil.post(URL_VIDEOADDRESS,paramsDevice,null);
        String code = jsonObject.getString("code");
        if(!"200".equals(code)){
            String errMsg = jsonObject.getString("msg");
            log.error("请求萤石云DeviceInfo错误：{}",errMsg);
            return null;
        }
        JSONObject data = jsonObject.getJSONObject("data");
        String url = data.getString("url");
        return url;
    }

}
