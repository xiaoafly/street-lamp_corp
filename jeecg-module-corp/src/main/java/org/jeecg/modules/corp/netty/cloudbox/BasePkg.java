package org.jeecg.modules.corp.netty.cloudbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BasePkg {
    private Integer code;
    /**消息序列号*/
    private Integer serial;

}
