package org.jeecg.modules.corp.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.corp.entity.Lamp;
import org.jeecg.modules.corp.netty.lamp.LampFrameContent;
import org.jeecg.modules.corp.service.ILampService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: tb_lamp
 * @Author: jeecg-boot
 * @Date:   2022-09-08
 * @Version: V1.0
 */
@Api(tags="tb_lamp")
@RestController
@RequestMapping("/corp/lamp")
@Slf4j
public class LampController extends JeecgController<Lamp, ILampService> {
	@Autowired
	private ILampService lampService;
	
	/**
	 * 分页列表查询
	 *
	 * @param lamp
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "tb_lamp-分页列表查询")
	@ApiOperation(value="tb_lamp-分页列表查询", notes="tb_lamp-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Lamp>> queryPageList(Lamp lamp,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Lamp> queryWrapper = QueryGenerator.initQueryWrapper(lamp, req.getParameterMap());
		Page<Lamp> page = new Page<Lamp>(pageNo, pageSize);
		IPage<Lamp> pageList = lampService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param lamp
	 * @return
	 */
	@AutoLog(value = "tb_lamp-添加")
	@ApiOperation(value="tb_lamp-添加", notes="tb_lamp-添加")
	//@RequiresPermissions("org.jeecg.modules:tb_lamp:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Lamp lamp) {
		lampService.save(lamp);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param lamp
	 * @return
	 */
	@AutoLog(value = "tb_lamp-编辑")
	@ApiOperation(value="tb_lamp-编辑", notes="tb_lamp-编辑")
	//@RequiresPermissions("org.jeecg.modules:tb_lamp:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Lamp lamp) {
		lampService.updateById(lamp);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "tb_lamp-通过id删除")
	@ApiOperation(value="tb_lamp-通过id删除", notes="tb_lamp-通过id删除")
	//@RequiresPermissions("org.jeecg.modules:tb_lamp:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		lampService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "tb_lamp-批量删除")
	@ApiOperation(value="tb_lamp-批量删除", notes="tb_lamp-批量删除")
	//@RequiresPermissions("org.jeecg.modules:tb_lamp:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.lampService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "tb_lamp-通过id查询")
	@ApiOperation(value="tb_lamp-通过id查询", notes="tb_lamp-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Lamp> queryById(@RequestParam(name="id",required=true) String id) {
		Lamp lamp = lampService.getById(id);
		if(lamp==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(lamp);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param lamp
    */
    //@RequiresPermissions("org.jeecg.modules:tb_lamp:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Lamp lamp) {
        return super.exportXls(request, lamp, Lamp.class, "tb_lamp");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_lamp:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Lamp.class);
    }
	 @AutoLog(value = "照明-（开关、亮度控制）")
	 @ApiOperation(value="照明-（开关、亮度控制）", notes="照明-（开关、亮度控制）")
	 @RequestMapping(value = "/control", method ={RequestMethod.POST,RequestMethod.GET})
	 public Result<?> control(@RequestParam(name="id",required=true) String id
			 ,@RequestParam(name="on",required=false) Integer on
			 ,@RequestParam(name="bri",required=false) Integer bri) throws JsonProcessingException {
		 Lamp lamp = lampService.getById(id);
		 if(lamp==null) {
			 return Result.error("未找到对应数据");
		 }
		 List<Number> contentList = new ArrayList<>();
		 if(on!=null){
		 	contentList.add(LampFrameContent.KGDK);
			 if(on==1){
				 contentList.add((byte)0x00);
			 }else{
				 contentList.add((byte)0x01);
			 }
		 }else if(bri!=null){
			contentList.add(LampFrameContent.TGDK);
			contentList.add(bri.byteValue());
		 }

		lampService.sendControlPackage(id,contentList);
		 return Result.OK();
	 }


	 @RequestMapping(value = "/allList", method = {RequestMethod.GET,RequestMethod.POST})
	 public Result<List<Lamp>> allList() {
		 QueryWrapper<Lamp> qw = new QueryWrapper();
		 qw.lambda().eq(true, Lamp::getOlStatus, "1");
		 return Result.OK(lampService.list(qw));
	 }

	 @AutoLog(value = "获取设备总览")
	 @ApiOperation(value="获取设备总览", notes="获取设备总览")
	 @RequestMapping(value = "/deviceOverview", method = {RequestMethod.GET,RequestMethod.POST})
	 public Result<Map> deviceOverview() {
		 QueryWrapper<Lamp> qw = new QueryWrapper();
		 qw.lambda().eq(true, Lamp::getOlStatus, "1");
		 Map map=new HashMap();
		 map.put("zm",lampService.count(qw));
		 map.put("jk","0");
		 map.put("cdz","0");
		 map.put("wifi","0");
		 map.put("xxpm","0");
		 map.put("yjjg","0");
		 map.put("gb","0");
		 map.put("cgq","0");
		 return Result.OK(map);
	 }

 }
