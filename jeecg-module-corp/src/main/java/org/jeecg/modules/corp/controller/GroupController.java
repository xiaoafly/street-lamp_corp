package org.jeecg.modules.corp.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.corp.entity.Group;
import org.jeecg.modules.corp.service.IGroupService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 设备分组
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
@Api(tags="设备分组")
@RestController
@RequestMapping("/corp/group")
@Slf4j
public class GroupController extends JeecgController<Group, IGroupService> {
	@Autowired
	private IGroupService groupService;
	
	/**
	 * 分页列表查询
	 *
	 * @param group
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "设备分组-分页列表查询")
	@ApiOperation(value="设备分组-分页列表查询", notes="设备分组-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Group>> queryPageList(Group group,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Group> queryWrapper = QueryGenerator.initQueryWrapper(group, req.getParameterMap());
		Page<Group> page = new Page<Group>(pageNo, pageSize);
		IPage<Group> pageList = groupService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param group
	 * @return
	 */
	@AutoLog(value = "设备分组-添加")
	@ApiOperation(value="设备分组-添加", notes="设备分组-添加")
	//@RequiresPermissions("org.jeecg.modules:tb_group:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Group group) {
		groupService.save(group);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param group
	 * @return
	 */
	@AutoLog(value = "设备分组-编辑")
	@ApiOperation(value="设备分组-编辑", notes="设备分组-编辑")
	//@RequiresPermissions("org.jeecg.modules:tb_group:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Group group) {
		groupService.updateById(group);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "设备分组-通过id删除")
	@ApiOperation(value="设备分组-通过id删除", notes="设备分组-通过id删除")
	//@RequiresPermissions("org.jeecg.modules:tb_group:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		groupService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "设备分组-批量删除")
	@ApiOperation(value="设备分组-批量删除", notes="设备分组-批量删除")
	//@RequiresPermissions("org.jeecg.modules:tb_group:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.groupService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "设备分组-通过id查询")
	@ApiOperation(value="设备分组-通过id查询", notes="设备分组-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Group> queryById(@RequestParam(name="id",required=true) String id) {
		Group group = groupService.getById(id);
		if(group==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(group);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param group
    */
    //@RequiresPermissions("org.jeecg.modules:tb_group:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Group group) {
        return super.exportXls(request, group, Group.class, "设备分组");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_group:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Group.class);
    }

}
