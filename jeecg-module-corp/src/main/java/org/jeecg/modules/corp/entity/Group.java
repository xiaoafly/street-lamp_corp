package org.jeecg.modules.corp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 设备分组
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
@Data
@TableName("tb_group")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_group对象", description="设备分组")
public class Group implements Serializable {
    private static final long serialVersionUID = 1L;

	/**分组ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "分组ID")
    private java.lang.String id;
	/**分组名称*/
	@Excel(name = "分组名称", width = 15)
    @ApiModelProperty(value = "分组名称")
    private java.lang.String groupName;
	/**分组说明*/
	@Excel(name = "分组说明", width = 15)
    @ApiModelProperty(value = "分组说明")
    private java.lang.String groupDesc;
	/**归属项目ID*/
    @Excel(name = "所属项目id", width = 15, dictTable = "tb_project", dicText = "project_name", dicCode = "id")
    @Dict(dictTable = "tb_project", dicText = "project_name", dicCode = "id")
    @ApiModelProperty(value = "归属项目ID")
    private java.lang.String projectId;
	/**创建人(用户名)*/
    @ApiModelProperty(value = "创建人(用户名)")
    private java.lang.String createBy;
	/**创建时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新人(用户名)*/
    @ApiModelProperty(value = "更新人(用户名)")
    private java.lang.String updateBy;
	/**更新时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**时间戳*/
	@Excel(name = "时间戳", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时间戳")
    private java.util.Date tmstamp;
}
