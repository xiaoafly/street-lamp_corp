package org.jeecg.modules.corp.netty.cloudbox;

import lombok.Data;

import java.util.Map;

@Data
public class Sladdr {
    private Map str;
    private String sladdr;
    private int sltype;
}
