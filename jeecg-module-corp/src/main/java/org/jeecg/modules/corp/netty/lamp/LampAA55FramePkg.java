package org.jeecg.modules.corp.netty.lamp;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * AA55帧头的数据指令
 */
@Data
public class LampAA55FramePkg {
    private int magic;
    private int playloadlen;
    private long reserved;
    private int crc16;
    private long deviceAddr;
    private short funCode;
    private Map<String,Number> pMap;

}
