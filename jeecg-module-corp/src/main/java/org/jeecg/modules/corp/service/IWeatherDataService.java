package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.WeatherData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 气象历史数据
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
public interface IWeatherDataService extends IService<WeatherData> {

}
