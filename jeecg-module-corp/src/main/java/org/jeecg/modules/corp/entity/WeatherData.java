package org.jeecg.modules.corp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 气象历史数据
 * @Author: jeecg-boot
 * @Date: 2022-10-20
 * @Version: V1.0
 */
@Data
@TableName("tb_weather_data")
//@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "tb_weather_data对象", description = "气象历史数据")
public class WeatherData implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 设备id
     */
    @Excel(name = "设备id", width = 15, dictTable = "tb_device", dicText = "name", dicCode = "id")
    @Dict(dictTable = "tb_device", dicText = "name", dicCode = "device_id")
    @ApiModelProperty(value = "设备id")
    private java.lang.String deviceId;
    /**
     * 最小风速，m/s
     */
    @Excel(name = "最小风速，m/s", width = 15)
    @ApiModelProperty(value = "最小风速，m/s")
    private java.lang.String minwspeed;
    /**
     * 平均风速，m/s
     */
    @Excel(name = "平均风速，m/s ", width = 15)
    @ApiModelProperty(value = "平均风速，m/s ")
    private java.lang.String avlwspeed;
    /**
     * 最大风速，m/s
     */
    @Excel(name = "最大风速，m/s ", width = 15)
    @ApiModelProperty(value = "最大风速，m/s ")
    private java.lang.String maxwspeed;
    /**
     * 最小风向，度
     */
    @Excel(name = "最小风向，度 ", width = 15)
    @ApiModelProperty(value = "最小风向，度 ")
    private java.lang.String minwdir;
    /**
     * 平均风向，度
     */
    @Excel(name = "平均风向，度", width = 15)
    @ApiModelProperty(value = "平均风向，度")
    private java.lang.String avlwdir;
    /**
     * 最大风向，度
     */
    @Excel(name = "最大风向，度 ", width = 15)
    @ApiModelProperty(value = "最大风向，度 ")
    private java.lang.String maxwdir;
    /**
     * 雨量，单位mm
     */
    @Excel(name = "雨量，单位mm ", width = 15)
    @ApiModelProperty(value = "雨量，单位mm ")
    private java.lang.String rains;
    /**
     * 气压，单位hPa
     */
    @Excel(name = "气压，单位hPa", width = 15)
    @ApiModelProperty(value = "气压，单位hPa")
    private java.lang.String airkPas;
    /**
     * 辐射量，单位 W/M2
     */
    @Excel(name = "辐射量，单位 W/M2", width = 15)
    @ApiModelProperty(value = "辐射量，单位 W/M2")
    private java.lang.String radiation;
    /**
     * 紫外线强度等级，单位UVI
     */
    @Excel(name = "紫外线强度等级，单位UVI", width = 15)
    @ApiModelProperty(value = "紫外线强度等级，单位UVI")
    private java.lang.String ultraviolet;
    /**
     * 单位dB
     */
    @Excel(name = "单位dB", width = 15)
    @ApiModelProperty(value = "单位dB")
    private java.lang.String noises;
    /**
     * pm25值, 0.1 ug/m3
     */
    @Excel(name = "pm25值, 0.1 ug/m3 ", width = 15)
    @ApiModelProperty(value = "pm25值, 0.1 ug/m3")
    private java.lang.String pm25s;
    /**
     * pm10值, 0.1 ug/m3
     */
    @Excel(name = "pm10值, 0.1 ug/m3", width = 15)
    @ApiModelProperty(value = "pm10值, 0.1 ug/m3")
    private java.lang.String pm1s;
    /**
     * 测量湿度，单位 %RH
     */
    @Excel(name = "测量湿度，单位 %RH", width = 15)
    @ApiModelProperty(value = "测量湿度，单位 %RH")
    private java.lang.String mhumis;
    /**
     * 测量温度
     */
    @Excel(name = "测量温度", width = 15)
    @ApiModelProperty(value = "测量温度")
    private java.lang.String mtemps;


    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
    /**
     * 时间戳
     */
    @Excel(name = "时间戳", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "时间戳")
    private java.util.Date tmstamp;
}
