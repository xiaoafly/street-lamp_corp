package org.jeecg.modules.corp.netty.lamp;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 查询单灯采集信息指令响应包
 */
@Data
public class LampQueryOneResp extends LampQueryResp{

    private Short tgdk;
    private Short tggnm;
    private Short tgz;
    private Short kgdk;
    private Short kggnm;
    private Short kgzt;
    private Short cjdk;
    private Short cjgnm;
    private Short cjsjyxcd;
    private Integer dyz;
    private Integer dlz;
    private Integer ygglz;
    private Integer wgglz;
    private Integer plz;
    private Integer wdz;

    private String imeiStr;




}
