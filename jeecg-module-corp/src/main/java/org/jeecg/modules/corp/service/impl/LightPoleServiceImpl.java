package org.jeecg.modules.corp.service.impl;

import org.jeecg.modules.corp.entity.LightPole;
import org.jeecg.modules.corp.mapper.LightPoleMapper;
import org.jeecg.modules.corp.service.ILightPoleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 灯杆管理
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
@Service
public class LightPoleServiceImpl extends ServiceImpl<LightPoleMapper, LightPole> implements ILightPoleService {

}
