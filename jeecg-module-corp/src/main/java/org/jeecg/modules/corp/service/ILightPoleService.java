package org.jeecg.modules.corp.service;

import org.jeecg.modules.corp.entity.LightPole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 灯杆管理
 * @Author: jeecg-boot
 * @Date:   2022-10-20
 * @Version: V1.0
 */
public interface ILightPoleService extends IService<LightPole> {

}
