package org.jeecg.modules.corp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.corp.entity.Project;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目信息
 * @Author: jeecg-boot
 * @Date:   2022-09-23
 * @Version: V1.0
 */
public interface ProjectMapper extends BaseMapper<Project> {

}
