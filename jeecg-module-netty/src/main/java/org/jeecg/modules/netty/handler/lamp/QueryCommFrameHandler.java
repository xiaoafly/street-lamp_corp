package org.jeecg.modules.netty.handler.lamp;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.corp.netty.lamp.LampAA55FramePkg;
import org.jeecg.modules.corp.netty.lamp.LampQueryComm;
import org.jeecg.modules.corp.netty.lamp.LampQueryResp;

@Slf4j
public class QueryCommFrameHandler extends SimpleChannelInboundHandler<LampQueryResp> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LampQueryResp frame) throws Exception {
        log.info("收到灯控设备查询返回数据：{}",frame.toString());
        QueryCommFrameExec.execData(ctx,frame);

    }
}
