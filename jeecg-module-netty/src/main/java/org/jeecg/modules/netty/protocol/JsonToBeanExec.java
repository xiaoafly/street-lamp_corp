package org.jeecg.modules.netty.protocol;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.corp.netty.NettyChannelContainer;
import org.jeecg.modules.corp.netty.CloudBoxPackageContainer;
import org.jeecg.modules.corp.netty.cloudbox.PackageConstant;
import org.jeecg.modules.corp.netty.cloudbox.BasePkg;
import org.jeecg.modules.corp.netty.cloudbox.HeartPkg;
import org.jeecg.modules.corp.netty.cloudbox.RegisPkg;
import org.jeecg.modules.corp.netty.cloudbox.StatusChangePkg;

@Slf4j
public class JsonToBeanExec {

    /**
     *
     * @param ctx
     * @param jsonStr
     */
    public static void execData(ChannelHandlerContext ctx, String jsonStr) throws JsonProcessingException {
        if(StringUtils.isBlank(jsonStr)){
            return;
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        BasePkg base = mapper.readValue(jsonStr, BasePkg.class);
        if(base.getCode()==null){//注册包
            execRegis(ctx,jsonStr);
        }else if (base.getCode().intValue()== PackageConstant.HEART_CODE){
            execHeart(ctx,jsonStr);
        }else if(base.getCode().intValue() == PackageConstant.DEVICE_CHANGE){
            execStatusChange(ctx,jsonStr);
        }
    }

    /**
     * 心跳包处理
     * @param ctx
     * @param jsonStr
     * @throws JsonProcessingException
     */
    private static void execHeart(ChannelHandlerContext ctx, String jsonStr) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        HeartPkg heart = mapper.readValue(jsonStr, HeartPkg.class);
        if(heart==null){
            return;
        }
        log.info("收到心跳包：");
        CloudBoxPackageContainer.putPackage(heart);

        String playload = String.format("{\"code\":%d,\"result\":0,\"timestamp\":%d}",
                PackageConstant.HEART_RESP_CODE, System.currentTimeMillis()/1000);
        //v4
        //ctx.writeAndFlush(LampFrameContent.createPackage(playload));
        //v1
        ctx.writeAndFlush(playload);
    }

    /**
     * 网关注册包处理
     * @param ctx
     * @param jsonStr
     * @throws JsonProcessingException
     */
    private static void execRegis(ChannelHandlerContext ctx, String jsonStr) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        RegisPkg regis = mapper.readValue(jsonStr, RegisPkg.class);
        if(regis==null){
            return;
        }
        log.info("收到注册包：");
        if(!NettyChannelContainer.containKey(regis.getMac())){
            NettyChannelContainer.add(regis.getMac(),ctx.channel());
        }
        String playload = String.format("{\"timestamp\":%d}", System.currentTimeMillis()/1000);
        //v4
        //ctx.writeAndFlush(LampFrameContent.createPackage(playload));
        //v1
        ctx.writeAndFlush(playload);
    }

    /**
     * 设备状态改变包处理
     * @param ctx
     * @param jsonStr
     * @throws JsonProcessingException
     */
    private static void execStatusChange(ChannelHandlerContext ctx, String jsonStr) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        StatusChangePkg changePkg = mapper.readValue(jsonStr, StatusChangePkg.class);
        if(changePkg==null){
            return;
        }
        log.info("收到设备状态改变包：");
        CloudBoxPackageContainer.putPackage(changePkg);

        String playload = String.format("{\"code\":%d,\"control\":%d,\"id\":%s,\"ep\":%d,\"result\":0,}",
                PackageConstant.DEVICE_CHANGE_RESP, changePkg.getControl(),changePkg.getId(),changePkg.getEp());

        ctx.writeAndFlush(playload);
    }

}
