package org.jeecg.modules.netty.handler.v1;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.netty.protocol.JsonToBeanExec;

@Slf4j
public class JsonDataHandler extends SimpleChannelInboundHandler<String> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String jsonStr) throws Exception {
        log.info("channelRead0:"+jsonStr);
        JsonToBeanExec.execData(ctx,jsonStr);
    }
}
