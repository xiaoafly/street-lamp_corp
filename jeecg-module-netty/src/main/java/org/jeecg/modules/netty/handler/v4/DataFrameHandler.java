package org.jeecg.modules.netty.handler.v4;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.netty.protocol.JsonToBeanExec;
import org.jeecg.modules.netty.protocol.v4.DataFrame;

@Slf4j
public class DataFrameHandler extends SimpleChannelInboundHandler<DataFrame> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DataFrame dataFrame) throws Exception {
        log.info(dataFrame.toString());
        JsonToBeanExec.execData(ctx,dataFrame.getPlayload());

    }
}
