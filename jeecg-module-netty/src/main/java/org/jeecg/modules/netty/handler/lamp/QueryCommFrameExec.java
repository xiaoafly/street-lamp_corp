package org.jeecg.modules.netty.handler.lamp;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.corp.netty.LampPackageContainer;
import org.jeecg.modules.corp.netty.NettyChannelContainer;
import org.jeecg.modules.corp.netty.lamp.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class QueryCommFrameExec {

    /**
     *
     * @param ctx
     * @param pkg
     */
    public static void execData(ChannelHandlerContext ctx, LampQueryResp pkg) throws JsonProcessingException {
        if(pkg==null){
            return;
        }
        int magic = pkg.getMagic();
        if(LampFrameContent.QUERY_ONE_MAGIC_RESP==magic){
            LampQueryOneResp frame = (LampQueryOneResp) pkg;
            execCollectInfo(ctx,frame);
        }else {

        }



    }

    /**
     * 查询单灯采集信息指令响应包处理
     * @param ctx
     * @param frame
     */
    private static void execCollectInfo(ChannelHandlerContext ctx, LampQueryOneResp frame){
        log.info("收到单灯采集信息指令响应包");
        String imeiStr = ctx.channel().attr(LampFrameContent.IMEI_ATTR).get();
        frame.setImeiStr(imeiStr);
        LampPackageContainer.putPackage(frame);

    }



}
