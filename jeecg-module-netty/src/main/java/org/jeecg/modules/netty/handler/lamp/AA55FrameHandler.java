package org.jeecg.modules.netty.handler.lamp;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.corp.netty.lamp.LampAA55FramePkg;

@Slf4j
public class AA55FrameHandler extends SimpleChannelInboundHandler<LampAA55FramePkg> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LampAA55FramePkg frame) throws Exception {
        log.info("收到灯控设备AA55帧头数据：{}",frame.toString());
        AA55FrameExec.execData(ctx,frame);

    }
}
