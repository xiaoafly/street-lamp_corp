package org.jeecg.modules.netty.handler.v1;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.nio.charset.StandardCharsets;

public class JsonStrToByteEncoder extends MessageToByteEncoder<String> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, String s, ByteBuf byteBuf) throws Exception {
        if(s==null&&s.trim().length()==0){
            return;
        }
        byteBuf.writeCharSequence(s, StandardCharsets.UTF_8);
    }
}