package org.jeecg.modules.netty.protocol.v4;

/**
 * tcp协议包的通用Frame数据
 * 数据包协议格式
 * -----------------------------------|---------
 * | header（V1 版本无此部分， V4 版本有) | payload
 * ----------------------------------|----------
 *  数据包头（header）
 * --------|---------|-------|-------|----|------------
 * |Bytes:2|2        |1      |1      |1   | 1      |2
 * --------|---------|-------|-------|----|------------
 * |magic |payloadlen|version|enctype|type|reserved|crc
 * -------|----------|-------|-------|----|--------|---
 */
public class DataFrame {
    private int magic;
    private int playloadlen;
    private byte verion;
    private byte enctype;
    private byte type;
    private byte reserved;
    private int crc;
    private String playload;


    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public int getPlayloadlen() {
        return playloadlen;
    }

    public void setPlayloadlen(int playloadlen) {
        this.playloadlen = playloadlen;
    }

    public byte getVerion() {
        return verion;
    }

    public void setVerion(byte verion) {
        this.verion = verion;
    }

    public byte getEnctype() {
        return enctype;
    }

    public void setEnctype(byte enctype) {
        this.enctype = enctype;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getReserved() {
        return reserved;
    }

    public void setReserved(byte reserved) {
        this.reserved = reserved;
    }

    public int getCrc() {
        return crc;
    }

    public void setCrc(int crc) {
        this.crc = crc;
    }

    public String getPlayload() {
        return playload;
    }

    public void setPlayload(String playload) {
        this.playload = playload;
    }

    @Override
    public String toString() {
        return "{" +
                "magic:" + magic +
                ", playloadlen:" + playloadlen +
                ", verion:" + verion +
                ", enctype:" + enctype +
                ", type:" + type +
                ", reserved:" + reserved +
                ", crc:" + crc +
                ", playload:" + playload  +
                "}";
    }
}
