package org.jeecg.modules.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.netty.handler.TcpRequestHandler;
import org.jeecg.modules.netty.handler.lamp.AA55FrameHandler;
import org.jeecg.modules.netty.handler.lamp.ByteToLampFrameDecoder;
import org.jeecg.modules.netty.handler.lamp.LampQueryCommToByteEncoder;
import org.jeecg.modules.netty.handler.lamp.QueryCommFrameHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
@Slf4j
@Component
public class NettyLampRunner implements ApplicationRunner, ApplicationListener<ContextClosedEvent>{

    @Value("${netty.lamp.port}")
    private int port;

    @Value("${netty.lamp.ip}")
    private String ip;


    @Value("${netty.lamp.max-frame-size}")
    private long maxFrameSize;

    private Thread lampT;

    private Channel serverChannel;



    public void run(ApplicationArguments args){
        start();
    }

    private void start(){
        stopThread();
        lampT = new Thread(null,new LampRunner(),"Lamp 监听服务");
        lampT.start();
    }

    public void onApplicationEvent(ContextClosedEvent event) {
        if (this.serverChannel != null) {
            this.serverChannel.close();
        }
        log.info("lamp协议的Socket 服务停止");

    }

    private void stopThread(){
        if(lampT==null){
            return;
        }
        if(lampT.isAlive()){
            lampT.interrupt();
        }
        lampT = null;
    }


    class LampRunner implements Runnable{


        @Override
        public void run() {
            EventLoopGroup bossGroup = new NioEventLoopGroup();
            EventLoopGroup workerGroup = new NioEventLoopGroup();
            try {
                ServerBootstrap serverBootstrap = new ServerBootstrap();
                serverBootstrap.group(bossGroup, workerGroup);
                serverBootstrap.channel(NioServerSocketChannel.class);
                serverBootstrap.localAddress(new InetSocketAddress(ip, port));
                serverBootstrap.option(ChannelOption.SO_BACKLOG, 128)
                        .childOption(ChannelOption.SO_KEEPALIVE, true);
                serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();

                        pipeline.addLast(new TcpRequestHandler())
                                .addLast(new LoggingHandler(LogLevel.INFO))
                                //lamp协议
                                .addLast(new LampQueryCommToByteEncoder())
                                .addLast(new ByteToLampFrameDecoder())
                                .addLast(new AA55FrameHandler())
                                .addLast(new QueryCommFrameHandler())

                        ;
                    }
                });
                Channel channel = serverBootstrap.bind().sync().channel();
                serverChannel = channel;
                log.info("lamp协议的Socket 服务启动，ip={},port={}", ip, port);

                channel.closeFuture().sync();
            } catch (InterruptedException e) {
                log.error(e.getMessage(),e);
            } finally {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            }
        }
    }

}
