package org.jeecg.modules.netty.handler.v4;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.jeecg.modules.netty.protocol.v4.DataFrame;
import org.jeecg.modules.netty.protocol.v4.DataFrameUtil;


public class DataFrameToByteEncoder extends MessageToByteEncoder<DataFrame> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, DataFrame frame, ByteBuf byteBuf) throws Exception {
        byteBuf.writeShort(frame.getMagic());
        byteBuf.writeShort(frame.getPlayloadlen());
        byteBuf.writeByte(frame.getVerion());
        byteBuf.writeByte(frame.getEnctype());
        byteBuf.writeByte(frame.getType());
        byteBuf.writeByte(frame.getReserved());
        byteBuf.writeShort(frame.getCrc());
        byteBuf.writeBytes(frame.getPlayload().getBytes(DataFrameUtil.CHARSET));

    }

}
