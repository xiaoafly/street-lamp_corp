package org.jeecg.modules.netty.handler.v1;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class ByteToStringDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        int size = byteBuf.readableBytes();
        if(size>0){
            byte[] bytes = new byte[size];
            byteBuf.readBytes(bytes);
            String jsonStr = new String(bytes);
            list.add(jsonStr);
            log.info("Decoder Json:"+jsonStr);
        }

    }
}
