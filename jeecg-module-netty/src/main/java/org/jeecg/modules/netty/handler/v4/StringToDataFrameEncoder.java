package org.jeecg.modules.netty.handler.v4;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.netty.protocol.v4.DataFrameUtil;

import java.util.List;

public class StringToDataFrameEncoder extends MessageToMessageEncoder<String> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, String s, List<Object> list) throws Exception {
        if(StringUtils.isBlank(s)){
            return;
        }
        list.add(DataFrameUtil.createPackage(s));
    }
}
